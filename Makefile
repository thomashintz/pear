pear: pear.scm macros.scm
	csc -O3 pear.scm

pearfast: pear.scm macros.scm
	csc -O4 -lfa2 -block -C '-O2' pear.scm

run:
	./pear

runfast:
	./pear -:s256k -:h1g

(import chicken scheme)
(use (prefix glfw3 glfw:) (prefix opengl-glew gl:) gl-utils-core miscmacros srfi-4 gl-math
     lolevel posix posix-extras)
(use srfi-19)
(change-directory "/home/tjhintz/src/scheme/pear")
(include "macros")

(glfw:key-callback (lambda (window key scancode action mods)
                     (cond
                      ((and (eq? key glfw:+key-escape+) (eq? action glfw:+press+))
                       (glfw:set-window-should-close window #t)))))

(: *vertex* string)
(define *vertex*
#<<END
#version 330 core
layout (location = 0) in vec3 position;
uniform mat4 MVP;

void main ()
{
  gl_Position = MVP * vec4(position, 1.0);
  //gl_Position = vec4(position, 1.0);
}
END
)

(: *fragment* string)
(define *fragment*
#<<END
#version 330 core
uniform vec3 incolor;

out vec4 color;

void main()
{
  color = vec4(incolor, 0.8);
}
END
)

(cond-expand (csi (define *recompile-shaders?* #f))
             (else (void)))

(: *rect-vertices* f32vector)
(load "model.scm")

(define (eye-position vd vu eyesep)
  (let ((r (make-f32vector 16)))
    (set! r (cross-product vd vu))
    (normalize! r)

    (v-set! r
            (0 (/ (* (@ r 0) eyesep) 2.0))
            (1 (/ (* (@ r 1) eyesep) 2.0))
            (2 (/ (* (@ r 2) eyesep) 2.0)))
    r))

(define (projection screen-width screen-height aperature
                    eyesep focal-length)
  (let* ((ratio (/ screen-width screen-height 1.0))
         ;; (near 75.0)
         (near (/ focal-length 5.0)) ; clip to avoid extreme stereo
         (far 10000.0)
         (radians (degrees->radians (/ aperature 2.0)))
         (wd2 (* near (tan radians)))
         (ndfl (/ near focal-length 1.0))
         (dist 0.0)
         (scale 0.0)
         (left 0.0)
         (right 0.0)
         (top 0.0)
         (bottom 0.0))

    (set! left (- (- (* ratio wd2) (* 0.5 eyesep ndfl))))
    (set! right (- (* ratio wd2) (* 0.5 eyesep ndfl)))
    (set! top wd2)
    (set! bottom (- wd2))

    (frustum left right bottom top near far)))

;; make sure to use the correct type
(define view-matrix (lambda (a b) (void)))
(define model-matrix (lambda (a b c) (void)))
(define projection-matrix (lambda () (void)))
(define mvp (lambda () (void)))

(define (enable-features . r) (map (lambda (f) (gl:enable f)) r))

(define (run #!key width height (fullscreen #f) (stereo #f)
             focal-length (vsync #t))
  (with-cleanup window-gc
    (set! model-matrix
      (let* ((model (make-f32vector/cleanup window-gc 16 #f))
             (modelp (f32vector->pointer model))
             (vec (make-f32vector/cleanup window-gc 4 #f))
             (vecp (f32vector->pointer vec)))
        (pointer-mat4-identity modelp)
        (fxrepeat* i 4 (! vec i 0))
        (pointer-3d-scaling 4.0 4.0 4.0 modelp)
        ;; (! vec 3 -400.0)
        ;; (f32vector-set! vec 0 -0.1)
        ;; (f32vector-set! vec 1 0.1)
        ;; (pointer-translation vecp modelp)
        (lambda ()
          modelp)))

    (set! view-matrix
      (let* ((eye (make-f32vector/cleanup window-gc 3))
             (vp #f32(39 53 22))
             (vd #f32(-39 -53 -22))
             (vu #f32(0 1 0))
             (p (make-f32vector 3))
             (d (make-f32vector 3))
             (u (make-f32vector 3))
             (r (make-f32vector 3)))
        (lambda (op focal-length)
          (set! r (eye-position vd vu (/ focal-length 20.0)))
          (v-set! p
                  (0 (op (@ vp 0) (@ r 0)))
                  (1 (op (@ vp 1) (@ r 1)))
                  (2 (op (@ vp 2) (@ r 2))))
          (v-set! d
                  (0 (+ (op (@ vp 0) (@ r 0)) (@ vd 0)))
                  (1 (+ (op (@ vp 1) (@ r 1)) (@ vd 1)))
                  (2 (+ (op (@ vp 2) (@ r 2)) (@ vd 2))))
          (f32vector->pointer
           (look-at p d vu)))))

    (set! projection-matrix
      (lambda (focal-length width height)
        (f32vector->pointer   ; aperature     eyesep
         (projection width height 40.0 (/ focal-length 20.0) focal-length))))

    (set! mvp
      (let* ((model-view (make-f32vector/cleanup window-gc 16))
             (model-viewp (f32vector->pointer model-view))
             (mvp (make-f32vector/cleanup window-gc 16))
             (mvpp (f32vector->pointer mvp)))
        (lambda (focal-length op width height)
          (pointer-m* (projection-matrix focal-length width height)
                      (pointer-m* (view-matrix op focal-length)
                                  (model-matrix) model-viewp)
                      mvpp)
          mvp)))

    (glfw:with-window/safe
      (width height "example" resizeable: #f
             ;; swap-interval: (if vsync 1 0)
             ;; refresh-rate: 120
             stereo: stereo
             swap-interval: 1
             fullscreen: fullscreen
             context-version-major: 3
             context-version-minor: 3)
     (gl:init)
     (enable-features gl:+multisample+ gl:+depth-test+ gl:+blend+)
     (gl:cull-face gl:+back+)
     (gl:blend-func gl:+src-alpha+ gl:+one-minus-src-alpha+)
     (let* ((start-time (glfw:get-time))
            (frames 0)
            (shaders (list (make-shader gl:+vertex-shader+ *vertex*)
                           (make-shader gl:+fragment-shader+ *fragment*)))
            (program (make-program shaders))
            (total-frames 40000)
            (window (glfw:window))

            (vbo (gen-buffer))
            (vao (gen-vertex-array))
            (uniform-loc (gl:get-uniform-location program "MVP"))
            (color-loc (gl:get-uniform-location program "incolor")))

       (window-gc (lambda () (delete-vertex-array vao)))
       (window-gc (lambda () (delete-buffer vbo)))

       (with-vertex-array vao
         (gl:bind-buffer gl:+array-buffer+ vbo)
         (window-gc (lambda () (gl:bind-buffer gl:+array-buffer+ 0)))

         (gl:buffer-data gl:+array-buffer+
                         (size *rect-vertices*)
                         (f32vector->pointer *rect-vertices*)
                         gl:+stream-draw+)
         (gl:vertex-attrib-pointer 0 3 gl:+float+ #f 0 (address->pointer 0))
         (gl:enable-vertex-attrib-array 0))

       (time
        (dountil
         (or (fx= total-frames 0) (glfw:window-should-close window))

         (set! frames (fx+ frames 1))
         (set! total-frames (fx- total-frames 1))

         (let ((current-time (glfw:get-time)))
           (when (fp> (fp- current-time start-time) 1.0)
             (glfw:poll-events)
             (cond-expand
               (csi
                (when *recompile-shaders?*
                  (print "recompiling shaders")
                  (set! *recompile-shaders?* #f)
                  (for-each (lambda (s)
                              (gl:detach-shader program s)
                              (gl:delete-shader s))
                            shaders)
                  (set! shaders (list (make-shader gl:+vertex-shader+ *vertex*)
                                      (make-shader gl:+fragment-shader+ *fragment*)))
                  (make-program shaders program)))
               (else
                (print (/ frames (fp- current-time start-time)))))

             (set! start-time current-time)
             (set! frames 0)))

         ;;; render
         (gl:clear-color 0.0 0.0 0.0 1.0)
         (gl:clear (bitwise-ior gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
         (gl:use-program program)

         (gl:uniform-matrix4fv uniform-loc
                               1 #f
                               (mvp focal-length + width height))
         (gl:uniform3fv color-loc
                        1
                        #f32(0.0 100.0 0.0))
         (with-vertex-array vao
           (gl:draw-arrays gl:+triangles+ 0 36))
         (glfw:swap-buffers window)


         (gl:clear-color 0.0 0.0 0.0 1.0)
         (gl:clear (bitwise-ior gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
         (gl:uniform-matrix4fv uniform-loc
                               1 #f
                               (mvp focal-length - width height))
         (gl:uniform3fv color-loc
                        1
                        #f32(100.0 0.0 0.0))
         (with-vertex-array vao
           (gl:draw-arrays gl:+triangles+ 0 36))

         (glfw:swap-buffers window)
         ;;; end render

         ))))))

(cond-expand
  (compiling (run width: 1280 height: 720 fullscreen: #t stereo: #f
                  focal-length: 70.0 vysnc: #t))
  (else
   (use srfi-18)
   (define *window-thread* '())
   (define (start)
     (set! *window-thread*
       (make-thread (lambda ()
                      (run width: 800 height: 600 fullscreen: #t stereo: #f
                           focal-length: 70.0))))
     (thread-start! *window-thread*))
   ;; (define (stop) (glfw:set-window-should-close *window-thread* #t))
   ))

(define-syntax fxrepeat
  (syntax-rules ()
    ((_ n body ...)
     (fxrepeat i body ...))
    ((_ (var) n body ...)
     (let loop ((var n))
       (when (fx< var n)
             body ...
             (loop (fx+ var 1)))))))

(define-syntax @
  (syntax-rules ()
    ((_ vec i)
     (f32vector-ref vec i))))

(define-syntax !
  (syntax-rules ()
    ((_ vec i x)
     (f32vector-set! vec i x))))

;; Expands into a list of f32vector-set! The first argument is the
;; vector the next argument is a list of lists. A sublist contains an
;; index and a value that will be set at that index.
;;
;; NOTE: the only thing that is renamed is the (begin) surrounding the
;; group of f32vector-set!s.

;; example:
;; (v-set! vector (0 (+ 1.0 0.5) (1 2.0) (5 10.0))
;; (begin (f32vector-set! vector 0 (+ 1.0 0.5))
;;        (f32vector-set! vector 1 2.0)
;;        (f32vector-set! vector 5 10.0))
(define-syntax v-set!
  (er-macro-transformer
   (lambda (exp r cmp)
     (let ((vector (cadr exp)))
       `(,(r 'begin)
         ,@(map (lambda (index/val)
                  `(f32vector-set! ,(cadr exp)
                                   ,(car index/val)
                                   ,(cadr index/val)
                                   ))
                (cddr exp)))))))

(define-syntax fxrepeat*
  (syntax-rules ()
    ((_ var n body ...)
     (let loop ((var (fx- n 1)))
       (when (fx>= var 0)
             body ...
             (loop (fx- var 1)))))))

(define-syntax dountil
  (syntax-rules ()
    ((_ test body ...)
     (let loop ()
       body ...
       (unless test
               (loop))))))

(define-syntax glfw:with-window/safe
  (syntax-rules ()
    ((_ (w h name . keys) body ...)
     (dynamic-wind
         (lambda ()
           (glfw:init)
           (glfw:make-window w h name . keys))
         (lambda ()
           body ...)
         (lambda ()
           (glfw:destroy-window (glfw:window))
           (glfw:terminate))))))

(define-syntax with-cleanup
  (syntax-rules ()
    ((_ on-cleanup-proc body ...)
     (let ((procs '()))
       (define on-cleanup-proc
         (lambda (proc) (set! procs (cons proc procs))))
       (dynamic-wind
           (lambda () (void))
           (lambda () body ...)
           (lambda ()
             (for-each
              (lambda (proc)
                (proc))
              procs)))))))

(define-syntax make-f32vector/cleanup
  (syntax-rules ()
    ((_ on-cleanup-proc len)
     (make-f32vector/cleanup on-cleanup-proc len #f))
    ((_ on-cleanup-proc len init)
     (let ((v (make-f32vector len init #t #f)))
       (on-cleanup-proc (lambda () (release-number-vector v)))
       v))))
